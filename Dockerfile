FROM python:3.5

WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/requirements.txt
RUN pip install -U -r requirements.txt
COPY . /usr/src/app
CMD ["python", "api/api.py"]

