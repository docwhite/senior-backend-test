# encoding: utf-8
"""Configuration."""
import logging
import os
from tornado.options import define, options

# pylint: disable=too-few-public-methods


class Options(object):
    """Options.

    Define the configuration options.

    Args:
        environment (str): The default environment to start on. Note that ENV variable will overwrite this.
        apikey (str): The default apikey to start the server.
    """
    def __init__(self,
                 environment='development',
                 apikey=''):
        self.define_option(option_name='env',
                           option_value=environment)

    @staticmethod
    def define_option(option_name,
                      option_value):
        """Define a configuration option."""
        var_environment = os.environ.get(option_name.upper(), None)
        default_value = option_value
        if var_environment:
            default_value = var_environment
        define(option_name, default=default_value, help="{} option.".format(option_name))
        logging.info("The %s is set to: %s", option_name, default_value)


def main():
    """Main."""
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")


if __name__ == '__main__':
    main()
