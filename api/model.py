"""Models to be used on autotagging."""
import asyncio
import logging
import random
from werkzeug.security import generate_password_hash, check_password_hash

from mappers import UserMapper


class PasswordEncrypt(object):
    """Password Encrypt."""

    @classmethod
    def encrypt(cls,
                raw_password: str):
        """Encrypt the password."""
        password = generate_password_hash(raw_password)
        return password

    @classmethod
    def check_password(cls,
                       hashed_password: str,
                       raw_password: str):
        """Check password."""
        try:
            auth = check_password_hash(hashed_password, raw_password)
        except:
            auth = False
        return auth


class User(UserMapper):
    """User domain object."""
    def __init__(self,
                 email='',
                 password=''):
        self.email: str = email
        self.password: str = self.encrypt_password(password)
        super(User, self).__init__()

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, email):
        self.__email = email

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        self.__password = password

    def encrypt_password(self, password):
        self.password = PasswordEncrypt.encrypt(password)

    def check_password(self, password):
        """Check the password against the user. Assumes true password is loaded."""
        return PasswordEncrypt.check_password(self.password, password)


async def main():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    res = User(email='test@wide-eyes.it',
               password='password')
    print(res)
    print(res.email)
    user = res
    uid = await user.insert()
    logging.info(uid)
    user.email = 'zero@wide-eyes.it'
    print(res.email)
    user.password = '134256'
    await user.update()
    print(user.db_id)
    user.email = 'test@wide-eyes.it'
    await user.find()
    print(user.password)
    print(user.db_id)

if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    result = loop.run_until_complete(main())